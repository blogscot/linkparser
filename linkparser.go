// Package linkparser accepts HTML files, pages, or io.Readers and returns a list of links contained within the document source.
package linkparser

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"strings"

	"golang.org/x/net/html"
)

// Link defines the parse HTML link structure
type Link struct {
	Href string
	Text string
}

func (l Link) String() string {
	return fmt.Sprintf("Link{\n  Href: %q,\n  Text: %q,\n}", l.Href, l.Text)
}

// ParseFile parses a HTML file and returns a slice of Links contained within.
func ParseFile(htmlFile string) ([]Link, error) {
	file, err := ioutil.ReadFile(htmlFile)
	if err != nil {
		return nil, fmt.Errorf("unable to open file: %v", err)
	}
	return Parse(bytes.NewReader(file))
}

// ParseHTML parses HTML page source returning a slice of Links contained within.
func ParseHTML(pageSource string) ([]Link, error) {
	return Parse(strings.NewReader(pageSource))
}

// Parse parses io.Reader returning a slice of Links contains within.
func Parse(r io.Reader) ([]Link, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return nil, fmt.Errorf("parsing failure: %v", err)
	}
	links := []Link{}
	process(doc, &links)
	return links, nil
}

func process(node *html.Node, links *[]Link) {
	if node.Type == html.ElementNode && node.Data == "a" {
		for _, a := range node.Attr {
			if a.Key == "href" {
				data := []string{}
				getData(node, &data)
				link := Link{a.Val, strings.Trim(strings.Join(data, " "), " ")}
				*links = append(*links, link)
				break
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		process(c, links)
	}
}

func getData(node *html.Node, text *[]string) {
	if node.Type == html.TextNode {
		*text = append(*text, strings.Trim(node.Data, "\n "))
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		getData(c, text)
	}
}
