package linkparser

import (
	"fmt"
	"testing"
)

func TestFileMissing(t *testing.T) {
	given := "wrong_name.html"
	_, err := ParseFile(given)

	description := "Input file is not found"
	expected := fmt.Sprintf("unable to open file: open %s: no such file or directory", given)

	if err.Error() != expected {
		t.Fatalf("Parse(%s, %s): expected %s, actual %s", given, description, expected, err)
	}
}

func TestExampleFile1(t *testing.T) {
	given := "ex1.html"
	links, _ := ParseFile(given)
	description := "Parse a link to another page"
	expected := Link{"/other-page", "A link to another page"}

	actual := links[0]
	if fmt.Sprintf("%q", actual) != fmt.Sprintf("%q", expected) {
		t.Fatalf("Parse(%s, %s): expected %q, actual %q", given, description, expected, actual)
	}
}

func TestExampleFile2(t *testing.T) {
	given := "ex2.html"
	links, _ := ParseFile(given)
	description := "Gophercises is on Github"
	expected := []Link{
		Link{"https://www.twitter.com/joncalhoun", "Check me out on twitter"},
		Link{"https://github.com/gophercises", "Gophercises is on Github !"},
	}

	for index := range links {
		if fmt.Sprintf("%q", links[index]) != fmt.Sprintf("%q", expected[index]) {
			t.Fatalf("Parse(%s, %s): expected %q, actual %q", given, description, expected[index], links[index])
		}
	}
}

func TestExampleFile3(t *testing.T) {
	given := "ex3.html"
	links, _ := ParseFile(given)
	description := "Lost? Need help?"
	expected := []Link{
		Link{"#", "Login"},
		Link{"/lost", "Lost? Need help?"},
		Link{"https://twitter.com/marcusolsson", "@marcusolsson"},
	}

	for index := range links {
		if fmt.Sprintf("%q", links[index]) != fmt.Sprintf("%q", expected[index]) {
			t.Fatalf("Parse(%s, %s): expected %q, actual %q", given, description, expected[index], links[index])
		}
	}
}

func TestExampleFile4(t *testing.T) {
	given := "ex4.html"
	links, _ := ParseFile(given)
	description := "Dog cat"
	expected := Link{"/dog-cat", "dog cat"}

	actual := links[0]
	if fmt.Sprintf("%q", actual) != fmt.Sprintf("%q", expected) {
		t.Fatalf("Parse(%s, %s): expected %q, actual %q", given, description, expected, actual)
	}
}

func TestHTMLSource1(t *testing.T) {
	given := `<html>

	<body>
		<h1>Hello!</h1>
		<a href="/other-page">A link to another page</a>
	</body>
	
	</html>`

	links, _ := ParseHTML(given)
	description := "Parse a link to another page"
	expected := Link{"/other-page", "A link to another page"}

	actual := links[0]
	if fmt.Sprintf("%q", actual) != fmt.Sprintf("%q", expected) {
		t.Fatalf("Parse(%s, %s): expected %q, actual %q", given, description, expected, actual)
	}
}
