package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	"github.com/blogscot/linkparser"
)

func main() {
	rawurl := flag.String("url", "https://www.sitemaps.org/", "the URL to parse")
	flag.Parse()
	Single(*rawurl)
}

// Single gets the links for a single page
func Single(rawurl string) {

	resp, err := http.Get(rawurl)
	if err != nil {
		log.Fatalf("failed to load url page: %v\n", err)
	}

	links, err := linkparser.Parse(resp.Body)
	if err != nil {
		log.Fatalf("error during parsing: %v\n", err)
	}

	for _, link := range links {
		fmt.Println(link)
	}
}
